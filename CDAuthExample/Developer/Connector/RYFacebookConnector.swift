//
//  RYFacebookConnector.swift
//  Rooky
//
//  Created by Yury Radchenko on 29.04.16.
//  Copyright © 2016 Cookiedev. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import Social

struct FBField {
    static let KeyFields = "fields"
    
    static let Email = "email"
    static let FirstName = "first_name"
    static let LastName = "last_name"
}

public struct RYFacebookAccount {
    var accountId = ""
    var token = ""
    var email = ""
    var firstName = ""
    var lastName = ""
    var imageURL = ""
}

class RYFacebookConnector: NSObject {
    
    private let loginManager = FBSDKLoginManager()
    private let facebookAccount = SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook)
    
    //MARK: - Init -
    // ******************************************************
    override init() {
        super.init()
        
        self.loginManager.logOut() //если до этого был уже вход под другим аккаунтом
        
        if self.facebookAccount {
            self.loginManager.loginBehavior = .SystemAccount
        } else {
            self.loginManager.loginBehavior = .Web
        }
    }
    
    //MARK: - Functions -
    func loginFromViewController(vc: UIViewController, completion:(RYResult, facebookAccount: RYFacebookAccount?) -> Void) {
        
        self.loginManager.logInWithReadPermissions(["public_profile", "email"], fromViewController: vc) { (result, error) -> Void in
            
            if error != nil {
                print ("Facebook login ERROR. Part 1: \(error)")

                let result = RYResult(success: false, errorCode: 0)
                dispatch_async(dispatch_get_main_queue()) {
                    completion(result, facebookAccount: nil)
                }
                return
            }
                
            else if result.isCancelled {
                print ("Facebook login Cancelled")
                
                let result = RYResult(success: false, errorCode: 0)
                dispatch_async(dispatch_get_main_queue()) {
                    completion(result, facebookAccount: nil)
                }
                return
            }
            
            if FBSDKAccessToken.currentAccessToken() != nil {
                
                var fbAccount = RYFacebookAccount()
                fbAccount.accountId = result.token.userID
                fbAccount.token = result.token.tokenString
                
                let fbRequest = FBSDKGraphRequest.init(graphPath: "me", parameters: [FBField.KeyFields : "\(FBField.Email),\(FBField.FirstName),\(FBField.LastName)"])
                fbRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
                    
                    if error != nil {
                        print ("Facebook login ERROR. Part 2:  \(error)")
                        let result = RYResult(success: false, errorCode: 0)
                        dispatch_async(dispatch_get_main_queue()) {
                            completion(result, facebookAccount: nil)
                        }
                        return
                        
                    } else if (result.isCancelled != nil) {
                        print ("Facebook login Cancelled Part 2")
                        let result = RYResult(success: false, errorCode: 0)
                        dispatch_async(dispatch_get_main_queue()) {
                            completion(result, facebookAccount: nil)
                        }
                        return
                    }
                    
                    print("FB result = \(result)")
                    
                    if let firstName = result[FBField.FirstName] as? String {
                        fbAccount.firstName = firstName
                    }
                    
                    if let lastName = result[FBField.LastName] as? String {
                        fbAccount.lastName = lastName
                    }
                    
                    if let email = result[FBField.Email] as? String {
                        fbAccount.email = email
                    }
                    
                    print("FB user: \(fbAccount.email), \(fbAccount.lastName)")
                    
                    fbAccount.imageURL = "https://graph.facebook.com/\(fbAccount.accountId)/picture?type=large"
                    
                    let result = RYResult(success: true, errorCode: 200)
                    dispatch_async(dispatch_get_main_queue()) {
                        completion(result, facebookAccount: fbAccount)
                    }
                })
                
            } else {
                
                let result = RYResult(success: false, errorCode: 0)
                dispatch_async(dispatch_get_main_queue()) {
                    completion(result, facebookAccount: nil)
                }
            }
        }
    }
}
