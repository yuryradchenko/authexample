//
//  RYGoogleConnector.swift
//  Rooky
//
//  Created by Yury Radchenko on 07.06.16.
//  Copyright © 2016 Cookiedev. All rights reserved.
//

import Foundation

public struct RYGoogleAccount {
    var accountId = ""
    var token = ""
    var email = ""
    var firstName = ""
    var lastName = ""
    var imageURL = ""
}

protocol RYGoogleConnectorDelegate {
    func connectToGoogle(result: RYResult, googleAccount: RYGoogleAccount?)
}

class RYGoogleConnector: NSObject {
    
    private let kGoogleClientID = "276206447432-obfdnib38li559s01fr7kssts6n4b4a2.apps.googleusercontent.com"
    private var fromViewController: UIViewController?
    private var googleAccount = RYGoogleAccount()
    var delegate: RYGoogleConnectorDelegate?
    
    //MARK: - Init -
    override init() {
        super.init()
        
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().clientID = self.kGoogleClientID
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        
        // Uncomment to automatically sign in the user.
        GIDSignIn.sharedInstance().signInSilently() //Бывает приводит к ошибке с кодом -4
    }
    
    //MARK: - Functions -
    func loginFromViewController(vc: UIViewController) {
        self.fromViewController = vc
        
        GIDSignIn.sharedInstance().signIn()
        
        /*
        if GIDSignIn.sharedInstance().hasAuthInKeychain(){
            print("hasAuthInKeychain TRUE")
            
            //let user = GIDSignIn.sharedInstance().currentUser
        } else{
            print("hasAuthInKeychain FALSE")
            
        }
         */
    }
}

//MARK: - GIDSignInUIDelegate -
extension RYGoogleConnector : GIDSignInUIDelegate {
    
    // The sign-in flow has finished selecting how to proceed, and the UI should no longer display
    // a spinner or other "please wait" element.
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        //print("func \(#function)")
        
    }
    
    // If implemented, this method will be invoked when sign in needs to display a view controller.
    // The view controller should be displayed modally (via UIViewController's |presentViewController|
    // method, and not pushed unto a navigation controller's stack.
    func signIn(signIn: GIDSignIn!, presentViewController viewController: UIViewController!) {
        
        self.fromViewController?.presentViewController(viewController, animated: true, completion: {
            //
        })
    }
    
    // If implemented, this method will be invoked when sign in needs to dismiss a view controller.
    // Typically, this should be implemented by calling |dismissViewController| on the passed
    // view controller.
    func signIn(signIn: GIDSignIn!, dismissViewController viewController: UIViewController!) {
        //print("func \(#function)")
        viewController.dismissViewControllerAnimated(true, completion: {
            //
        })
    }
}

//MARK: - GIDSignInDelegate -
extension RYGoogleConnector : GIDSignInDelegate {
    
    // The sign-in flow has finished and was successful if |error| is |nil|.
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!) {
        //print("func \(#function)")
        
        if (error == nil) {
            
            self.googleAccount.accountId = user.userID
            self.googleAccount.token = user.authentication.idToken
            self.googleAccount.firstName = user.profile.givenName
            self.googleAccount.lastName = user.profile.familyName
            self.googleAccount.email = user.profile.email
            if user.profile.hasImage {
                    self.googleAccount.imageURL = user.profile.imageURLWithDimension(120).absoluteString
            }
            
            let result = RYResult(success: true, errorCode: 200)
            self.delegate?.connectToGoogle(result, googleAccount: self.googleAccount)
            
        } else {
            switch error.code {
            case -5:
                let result = RYResult(success: false, errorCode: 0)
                self.delegate?.connectToGoogle(result, googleAccount: nil)
            default:
                print("\(error.localizedDescription)")
            }
        }
    }
    
    // Finished disconnecting |user| from the app successfully if |error| is |nil|.
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user: GIDGoogleUser!, withError error: NSError!) {
        //print("func \(#function)")
    }
}