//
//  RYVKontakteConnector.swift
//  Rooky
//
//  Created by Yury Radchenko on 10.05.16.
//  Copyright © 2016 Cookiedev. All rights reserved.
//

import Foundation
import VK_ios_sdk

protocol RYVKontakteConnectorDelegate {
    func connectToVK(result: RYResult, vkAccount: RYVKAccount?)
}

private struct VKField {
    static private let KeyFields = "fields"
    
    static private let FirstName = "first_name"
    static private let LastName = "last_name"
    static private let Photo50 = "photo_50"
}

public struct RYVKAccount {
    var accountId = ""
    var token = ""
    var email = ""
    var firstName = ""
    var lastName = ""
    var imageURL = ""
}

class RYVKontakteConnector: NSObject {
    
    var delegate: RYVKontakteConnectorDelegate?
    
    private let vkAppId = "5433922"
    
    private var fromViewController: UIViewController?
    private var vkAccount = RYVKAccount()
    
    //MARK: - Init -
    override init() {
        super.init()
        
        VKSdk.forceLogout() //если была сессия ранее - резать, иначе будут ошибки повторной авторизации
        
        let sdkVKInstance = VKSdk.initializeWithAppId(vkAppId)
        sdkVKInstance.registerDelegate(self)
        sdkVKInstance.uiDelegate = self
    }
    
    //MARK: - Functions -
    func loginFromViewController(vc: UIViewController) {
        
        self.fromViewController = vc
        
        let SCOPE = [VK_PER_EMAIL];
        
        VKSdk.wakeUpSession(SCOPE) { (authorizationState, error) in
            
            if authorizationState == VKAuthorizationState.Authorized {
                //print ("VK user Authorized")
                VKSdk.authorize(SCOPE)
                
            } else if error != nil {
                print (error)
                
            } else {
                //print ("VK user NOT Authorized")
                
                if VKSdk.vkAppMayExists() == true {
                    VKSdk.authorize(SCOPE) //([VK_PER_WALL])
                    
                } else {
                    //print("Приложения ВК нет")
                    VKSdk.authorize(SCOPE, withOptions: [.DisableSafariController, .UnlimitedToken])
                }
            }
        }
    }
    
    private func requestUserDataById(vkAccountId: String, dismiss: Bool) {
        
        let params = ["user_ids": vkAccountId, VKField.KeyFields: VKField.Photo50] as Dictionary <String, String>
        let request = VKRequest(method: "users.get", parameters: params)
        request.executeWithResultBlock(
            {
                (response) -> Void in
                
                let books =  response.json as! NSArray
                if let thePhoto: AnyObject = books[0].objectForKey(VKField.Photo50) {
                    self.vkAccount.imageURL = thePhoto as! String
                }
                
                if let theFirstName: AnyObject = books[0].objectForKey(VKField.FirstName) {
                    self.vkAccount.firstName = theFirstName as! String
                }
                
                if let theLastName: AnyObject = books[0].objectForKey(VKField.LastName) {
                    self.vkAccount.lastName = theLastName as! String
                }
                
                if dismiss {
                    let result = RYResult(success: true, errorCode: 200)
                    self.delegate?.connectToVK(result, vkAccount: self.vkAccount)
                } else if VKSdk.vkAppMayExists() {
                    let result = RYResult(success: true, errorCode: 200)
                    self.delegate?.connectToVK(result, vkAccount: self.vkAccount)
                }
                
            }, errorBlock: {
                (error) -> Void in
                print("error get VK User Data: \(error)")
                if dismiss {
                    let result = RYResult(success: false, errorCode: 0)
                    self.delegate?.connectToVK(result, vkAccount: nil)
                }
        })
    }
}

//MARK: - VKSdkDelegate -
extension RYVKontakteConnector: VKSdkDelegate {
    
    func vkSdkAccessAuthorizationFinishedWithResult(result: VKAuthorizationResult!) {
        
        switch result.state {
        case .Unknown:
             /// Authorization state unknown, probably ready to work or something went wrong
            print("result.state = .Unknown")
            
        case .Initialized:
            /// SDK initialized and ready to authorize
            print("result.state = .Initialized")
            
        case .Pending:
            /// Authorization state pending, probably we're trying to load auth information
            print("result.state = .Pending")
            
            let token: VKAccessToken = result.token
            //print("token.VKAccessToken = \(token)")
            if let accessToken = token.accessToken  {
                self.vkAccount.token = accessToken
            }
            
            if let userId = token.userId {
                self.vkAccount.accountId = userId
            }
            
            if let email = token.email {
                self.vkAccount.email = email
            }
            
            self.requestUserDataById(self.vkAccount.accountId, dismiss: false)
            
        case .External:
            /// Started external authorization process
            print("result.state = .External")
            
        case .SafariInApp:
            /// Started in app authorization process, using SafariViewController
            print("result.state = .SafariInApp")
            
        case .Webview:
            /// Started in app authorization process, using webview
            print("result.state = .Webview")
            
        case .Authorized:
            /// User authorized
            //print("result.state = .Authorized")
            
            let token: VKAccessToken = result.token
            
            if let accessToken = token.accessToken {
                self.vkAccount.token = accessToken
            }
            
            if let userId = token.userId {
                self.vkAccount.accountId = userId
            }
            
            if let email = token.email {
                self.vkAccount.email = email
            }
            
            self.requestUserDataById(self.vkAccount.accountId, dismiss: true)
            
        case .Error:
            /// An error occured, try to wake up session later
            //Например, отказ разрешений
            //print("result.state = .Error")
            if VKSdk.vkAppMayExists() {
                let result = RYResult(success: false, errorCode: 0)
                self.delegate?.connectToVK(result, vkAccount: nil)
            }
        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        //print("func \(#function)")
        let result = RYResult(success: false, errorCode: 0)
        self.delegate?.connectToVK(result, vkAccount: nil)
    }
    
    func vkSdkAuthorizationStateUpdatedWithResult(result: VKAuthorizationResult!) {
        //print("func \(#function)")
    }
    
    func vkSdkAccessTokenUpdated(newToken: VKAccessToken!, oldToken: VKAccessToken!) {
        //print("func \(#function)")
    }
    
    func vkSdkTokenHasExpired(expiredToken: VKAccessToken!) {
        //print("func \(#function)")
        //print("expiredToken = \(expiredToken)")
    }
}

//MARK: - VKSdkUIDelegate -
extension RYVKontakteConnector: VKSdkUIDelegate {
    
    func vkSdkShouldPresentViewController(controller: UIViewController!) {
        
        //print("func \(#function)")
        //Если приложение VK не установлено, или же схемы нет, то будет происходить вызов этого метода

        let shareDialogVC = VKShareDialogController.init()
        shareDialogVC.text =  "Rooky.pro сервис"
        
        shareDialogVC.dismissAutomatically = true
        shareDialogVC.requestedScope = [VK_PER_EMAIL, VK_PER_WALL]
        shareDialogVC.vkImages = ["-113119325_412838948", "-113119325_399333404", "-113119325_408607205", "-113119325_399456616", "-113119325_407522209"]
        
        shareDialogVC.completionHandler = {(shareDialogController, shareDialogControllerResult) -> Void in
            
            if self.vkAccount.accountId.countChars() > 0 {
                let result = RYResult(success: true, errorCode: 200)
                self.delegate?.connectToVK(result, vkAccount: self.vkAccount)
                
            } else {
                let result = RYResult(success: false, errorCode: 0)
                self.delegate?.connectToVK(result, vkAccount: nil)
            }
        }
        
        self.fromViewController?.navigationController?.presentViewController(shareDialogVC, animated: true, completion: {})
    }
    
    func vkSdkNeedCaptchaEnter(captchaError: VKError!) {
        //print("func \(#function)")
        let vc = VKCaptchaViewController.captchaControllerWithError(captchaError)
        vc.presentIn(self.fromViewController)
    }
    
    func vkSdkWillDismissViewController(controller: UIViewController!) {
        //print("func \(#function)")
    }
    
    func vkSdkDidDismissViewController(controller: UIViewController!) {
        //print("func \(#function)")
    }
}

