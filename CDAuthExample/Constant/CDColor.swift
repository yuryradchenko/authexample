//
//  CDColor.swift
//  CDAuthExample
//
//  Created by Yury Radchenko on 13.06.16.
//  Copyright © 2016 Cookiedev. All rights reserved.
//

import Foundation
import UIKit

enum CDColor {
    
    static let Blue = UIColor(rgba: "#00bbff")
    static let Red = UIColor(rgba: "#ff736a")
    static let Black = UIColor(rgba: "#333333")
    static let Green = UIColor(rgba: "#00C9B1")
    
    enum SocialButton {
        static let Facebook = UIColor(rgba: "#335099")
        static let VK = UIColor(rgba: "#4C75A3")
        static let Google = UIColor(rgba: "#CD3327")
    }
}