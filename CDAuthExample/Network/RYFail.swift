//
//  RYFail.swift
//  CDAuthExample
//
//  Created by Yury Radchenko on 13.06.16.
//  Copyright © 2016 Cookiedev. All rights reserved.
//

import UIKit

struct RYResult {
    var success = true
    var errorCode = 200
}

enum RYScope {
    case UserAuth
}

class RYFail: NSObject {
    class func fail(scope:RYScope, errorCode: Int)->String {
        
        var stringReturn = "Server Error"
        
        if errorCode == NSURLErrorNotConnectedToInternet {
            return "Not connected to internet"
        }
        
        switch scope {
        case .UserAuth:
            stringReturn =  "Error Authorization"
        }
        
        return stringReturn
    }
}
