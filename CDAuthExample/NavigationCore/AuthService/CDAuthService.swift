//
//  CDAuthService.swift
//  CDAuthExample
//
//  Created by Yury Radchenko on 13.06.16.
//  Copyright © 2016 Cookiedev. All rights reserved.
//

import Foundation

public enum RYAuthResult {
    case AuthSuccess, AuthNextStep, AuthFailed
}

//MARK: - Protocol -
protocol RYAuthSCServiceDelegate {
    func socialConnected(authResult: RYAuthResult, errorText: String?)
}


class CDAuthService: NSObject {

}
